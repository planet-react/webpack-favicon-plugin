const fs = require( 'fs' );
const glob = require( 'glob' );
const execSync = require( 'child_process' ).execSync;

const files = v => glob.sync( 'test/result/files/*.?(png|json|xml|ico|webapp)' );

if ( files().length > 0 ) {
    files().forEach( ( file ) => {
        fs.unlinkSync( file );
    } );
}

execSync( 'yarn build' );

// -----------------------------------------------------------------------------
let fails = 0;
let shouldExist = [ 'test/result/files/android-chrome-144x144.png',
    'test/result/files/android-chrome-192x192.png',
    'test/result/files/android-chrome-256x256.png',
    'test/result/files/android-chrome-36x36.png',
    'test/result/files/android-chrome-384x384.png',
    'test/result/files/android-chrome-48x48.png',
    'test/result/files/android-chrome-512x512.png',
    'test/result/files/android-chrome-72x72.png',
    'test/result/files/android-chrome-96x96.png',
    'test/result/files/apple-touch-icon-114x114.png',
    'test/result/files/apple-touch-icon-120x120.png',
    'test/result/files/apple-touch-icon-144x144.png',
    'test/result/files/apple-touch-icon-152x152.png',
    'test/result/files/apple-touch-icon-167x167.png',
    'test/result/files/apple-touch-icon-180x180.png',
    'test/result/files/apple-touch-icon-57x57.png',
    'test/result/files/apple-touch-icon-60x60.png',
    'test/result/files/apple-touch-icon-72x72.png',
    'test/result/files/apple-touch-icon-76x76.png',
    'test/result/files/apple-touch-icon-precomposed.png',
    'test/result/files/apple-touch-icon.png',
    'test/result/files/apple-touch-startup-image-1182x2208.png',
    'test/result/files/apple-touch-startup-image-1242x2148.png',
    'test/result/files/apple-touch-startup-image-1496x2048.png',
    'test/result/files/apple-touch-startup-image-1536x2008.png',
    'test/result/files/apple-touch-startup-image-320x460.png',
    'test/result/files/apple-touch-startup-image-640x1096.png',
    'test/result/files/apple-touch-startup-image-640x920.png',
    'test/result/files/apple-touch-startup-image-748x1024.png',
    'test/result/files/apple-touch-startup-image-750x1294.png',
    'test/result/files/apple-touch-startup-image-768x1004.png',
    'test/result/files/coast-228x228.png',
    'test/result/files/favicon-16x16.png',
    'test/result/files/favicon-32x32.png',
    'test/result/files/firefox_app_128x128.png',
    'test/result/files/firefox_app_512x512.png',
    'test/result/files/firefox_app_60x60.png',
    'test/result/files/mstile-144x144.png',
    'test/result/files/mstile-150x150.png',
    'test/result/files/mstile-310x150.png',
    'test/result/files/mstile-310x310.png',
    'test/result/files/mstile-70x70.png',
    'test/result/files/yandex-browser-50x50.png',
    'test/result/files/manifest.webapp',
    'test/result/files/manifest.json',
    'test/result/files/browserconfig.xml',
    'test/result/files/yandex-browser-manifest.json',
];

shouldExist.forEach( ( file ) => {
    if ( !fs.existsSync( file ) ) {
        console.log( 'FAIL: `' + file + '` not found' );
        fails++;
    }
} );

if ( fails > 0 ) {
    console.log( '----------------' );
    console.log( fails + ' tests failed.' );
    console.log( '----------------' );
} else {
    console.log( 'Test success' );
}

