const favicons = require( 'favicons' );
const fs = require( 'fs' );

const DEFAULTS = {
    appName: null,                  // Your application's name. `string`
    appDescription: null,           // Your application's description. `string`
    developerName: null,            // Your (or your developer's) name. `string`
    developerURL: null,             // Your (or your developer's) URL. `string`
    background: "#fff",             // Background colour for flattened icons. `string`
    theme_color: "#fff",            // Theme color for browser chrome. `string`
    path: "/",                      // Path for overriding default icons path. `string`
    display: "standalone",          // Android display: "browser" or "standalone". `string`
    orientation: "portrait",        // Android orientation: "portrait" or "landscape". `string`
    start_url: "/?homescreen=1",    // Android start application's URL. `string`
    version: "1.0",                 // Your application's version number. `number`
    logging: false,                 // Print logs to console? `boolean`
    online: false,                  // Use RealFaviconGenerator to create favicons? `boolean`
    preferOnline: false,            // Use offline generation, if online generation has failed. `boolean`
    icons: {
        // Platform Options:
        // - offset - offset in percentage
        // - shadow - drop shadow for Android icons, available online only
        // - background:
        //   * false - use default
        //   * true - force use default, e.g. set background for Android icons
        //   * color - set background for the specified icons
        //
        android: true,              // Create Android homescreen icon. `boolean` or `{ offset, background, shadow }`
        appleIcon: true,            // Create Apple touch icons. `boolean` or `{ offset, background }`
        appleStartup: true,         // Create Apple startup images. `boolean` or `{ offset, background }`
        coast: { offset: 25 },      // Create Opera Coast icon with offset 25%. `boolean` or `{ offset, background }`
        favicons: true,             // Create regular favicons. `boolean`
        firefox: true,              // Create Firefox OS icons. `boolean` or `{ offset, background }`
        windows: true,              // Create Windows 8 tile icons. `boolean` or `{ background }`
        yandex: true                // Create Yandex browser icon. `boolean` or `{ background }`
    }
};

/**
 * @param {{}} options
 * @constructor
 */
function FaviconPlugin( options ) {

    this.shouldRun = true;
    if ( !options.source ) {
        console.warn( 'Warning: [Favicon]: No `source` options specified.' );
        this.shouldRun = false;
    }

    if ( !options.output ) {
        console.warn( 'Warning: [Favicon]: No `output` options specified.' );
        this.shouldRun = false;
    }

    if ( !this.shouldRun ) {
        console.warn( 'Warning: [Favicon]: Missing configuration. Favicons will not be generated' )
    }

    this.source = options.source;
    this.configuration = Object.assign( {}, DEFAULTS, options );
}

FaviconPlugin.prototype.apply = function (compiler ) {

    compiler.plugin( 'compilation', ( compilation ) => {

        if ( !this.shouldRun ) {
            return;
        }

        compilation.plugin( 'html-webpack-plugin-before-html-processing', ( htmlPluginData, callback ) => {

            const headPattern = /<\/head>/;

            if ( headPattern.test( htmlPluginData.html ) ) {
                favicons( this.source, this.configuration, ( error, response ) => {
                    if ( error ) {
                        console.log( error.status );  // HTTP error code (e.g. `200`) or `null`
                        console.log( error.name );    // Error name e.g. "API Error"
                        console.log( error.message ); // Error description e.g. "An unknown error has occurred"
                        return;
                    }

                    // Generate favicons
                    let outputPath = this.configuration.output;
                    response.images.forEach( ( image ) => {
                        fs.writeFileSync( outputPath + '/' + image.name, image.contents );
                    } );

                    response.files.forEach( ( file ) => {
                        fs.writeFileSync( outputPath + '/' + file.name, file.contents );
                    } );

                    // Add meta tags to html webpack plugin file
                    const html = response.html.join( '\n\t' );
                    htmlPluginData.html = htmlPluginData.html.replace( headPattern, '\t' + html + '\n</head>' );

                    return callback( null, htmlPluginData );
                } );
            }
        } );
    } );
};

module.exports = FaviconPlugin;