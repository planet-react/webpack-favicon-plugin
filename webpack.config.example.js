const path = require( 'path' );
const webpack = require( 'webpack' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const FaviconPlugin = require( './index' );

module.exports = {
    entry: path.resolve( __dirname, 'test/src/index.js' ),
    output: {
        path: path.resolve( __dirname, 'test/result' ),
        filename: 'bundle.js',
    },

    module: {},

    plugins: [

        new HtmlWebpackPlugin( {
            title: 'Favicon Plugin',
            hash: true,
            filename: path.resolve( __dirname, 'test/result/index.html' )
        } ),

        /**
         * Create Favicons
         */
        new FaviconPlugin( {
            source: path.resolve( __dirname, 'test/src/favicon.png' ),
            output: path.resolve( __dirname, 'test/result/files' ),
            path: 'files',
            appName: 'Favicon Plugin',
            appDescription: 'Favicon Plugin',
        } )

    ]
};